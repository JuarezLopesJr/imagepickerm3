package com.example.imagepickerm3.data

import android.net.Uri
import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Image(
    val id: Long,
    val uri: Uri,
    val name: String
) : Parcelable