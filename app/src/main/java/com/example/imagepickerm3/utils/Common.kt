package com.example.imagepickerm3.utils

import android.content.ContentUris
import android.content.Context
import android.provider.MediaStore
import com.example.imagepickerm3.data.Image

fun retrieveMedia(context: Context): List<Image> {
    val projection = arrayOf(
        MediaStore.Images.Media._ID,
        MediaStore.Images.Media.DISPLAY_NAME
    )

    val images = mutableListOf<Image>()

    context.contentResolver.query(
        MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
        projection,
        null,
        null
    )?.use { cursor ->
        val idColumn = cursor.getColumnIndexOrThrow(MediaStore.Images.Media._ID)
        val nameColum = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DISPLAY_NAME)

        while (cursor.moveToNext()) {
            val id = cursor.getLong(idColumn)
            val name = cursor.getString(nameColum)

            val contentUri = ContentUris.withAppendedId(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                id
            )
            images.add(Image(id = id, uri = contentUri, name = name))
        }
    }
    return images
}

object Tag {
    const val TAG_IMAGE_GALLERY = "image_gallery"
    const val TAG_PROGRESS = "progress"
    const val TAG_DENIED_PERMISSION = "denied_permission"
    const val TAG_PERMISSIONS_BUTTON = "permissions_button"
    const val TAG_IMAGE_GRID = "image_grid"
    const val TAG_IMAGE = "image_"
    const val TAG_IMAGE_PREVIEW = "image_preview"
}