package com.example.imagepickerm3.ui.screens

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.testTag
import com.example.imagepickerm3.data.Image
import com.example.imagepickerm3.utils.Tag.TAG_DENIED_PERMISSION
import com.example.imagepickerm3.utils.Tag.TAG_IMAGE_GALLERY
import com.example.imagepickerm3.utils.Tag.TAG_PROGRESS
import com.example.imagepickerm3.utils.retrieveMedia
import com.google.accompanist.permissions.ExperimentalPermissionsApi
import com.google.accompanist.permissions.PermissionState
import com.google.accompanist.permissions.PermissionStatus
import com.google.accompanist.permissions.shouldShowRationale

@ExperimentalPermissionsApi
@Composable
fun GalleryContent(
    modifier: Modifier = Modifier,
    media: List<Image>? = null,
    permissionState: PermissionState,
    openSettings: () -> Unit
) {
    when (permissionState.status) {
        PermissionStatus.Granted -> {
            if (media == null) {
                Box(
                    modifier = modifier.testTag(TAG_PROGRESS),
                    contentAlignment = Alignment.Center
                ) {
                    CircularProgressIndicator()
                }
            } else {
                ImageGallery(
                    modifier = modifier.testTag(TAG_IMAGE_GALLERY),
                    images = retrieveMedia(LocalContext.current)
                )
            }
        }
        is PermissionStatus.Denied -> {
            if (permissionState.status.shouldShowRationale) {
                DeniedPermission(
                    modifier = Modifier.fillMaxSize().testTag(TAG_DENIED_PERMISSION),
                    handleLaunchSettings = {
                        openSettings()
                    }
                )
            } else {
                PermissionExplainer(
                    modifier = Modifier.fillMaxSize(),
                    requestPermission = {
                        permissionState.launchPermissionRequest()
                    }
                )
            }
        }
    }
}