package com.example.imagepickerm3.ui.screens

import android.Manifest
import android.content.Intent
import android.net.Uri
import android.provider.Settings
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import com.example.imagepickerm3.data.Image
import com.example.imagepickerm3.utils.retrieveMedia
import com.google.accompanist.permissions.ExperimentalPermissionsApi
import com.google.accompanist.permissions.isGranted
import com.google.accompanist.permissions.rememberPermissionState
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

@ExperimentalPermissionsApi
@Composable
fun GalleryScreen() {
    val permissionState = rememberPermissionState(
        permission = Manifest.permission.READ_EXTERNAL_STORAGE
    )

    val scope = rememberCoroutineScope()

    val context = LocalContext.current

    var retrievedMedia by rememberSaveable { mutableStateOf<List<Image>?>(null) }

    /* loading media only in the initial composition, if permission is granted */
    LaunchedEffect(key1 = permissionState.status.isGranted) {
        if (permissionState.status.isGranted) {
            scope.launch(Dispatchers.IO) {
                /* using IO context to not block UI */
                val retrieveMedia = retrieveMedia(context)

                /* changing context to return to the UI and display images */
                withContext(Dispatchers.Main) {
                    retrievedMedia = retrieveMedia
                }
            }
        }
    }

    GalleryContent(
        modifier = Modifier.fillMaxSize(),
        permissionState = permissionState,
        openSettings = {
            context.startActivity(
                Intent(
                    Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                    Uri.parse("package:${context.packageName}")
                )
            )
        }
    )
}