package com.example.imagepickerm3.ui.screens

import android.net.Uri
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.fadeIn
import androidx.compose.animation.fadeOut
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.foundation.lazy.grid.items
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import com.example.imagepickerm3.R
import com.example.imagepickerm3.data.Image
import com.example.imagepickerm3.utils.Tag.TAG_IMAGE
import com.example.imagepickerm3.utils.Tag.TAG_IMAGE_GRID
import com.example.imagepickerm3.utils.Tag.TAG_IMAGE_PREVIEW

@Composable
fun ImageGallery(
    modifier: Modifier = Modifier,
    images: List<Image>
) {
    var selectedImage by remember { mutableStateOf<Uri?>(null) }

    Box(contentAlignment = Alignment.Center) {
        LazyVerticalGrid(
            modifier = modifier.testTag(TAG_IMAGE_GRID),
            columns = GridCells.Fixed(2)
        ) {
            items(images) { image ->
                GalleryImage(
                    modifier = Modifier
                        .testTag(TAG_IMAGE + image.id)
                        .clickable(
                            onClickLabel = stringResource(id = R.string.cd_enlarge_image)
                        ) {
                            selectedImage = image.uri
                        }
                        .height(150.dp)
                        .fillMaxWidth(),
                    uri = image.uri
                )
            }
        }

        AnimatedVisibility(
            modifier = Modifier.fillMaxSize(),
            visible = selectedImage != null,
            enter = fadeIn(),
            exit = fadeOut()
        ) {
            selectedImage?.let {
                ImageSelected(
                    modifier = Modifier
                        .testTag(TAG_IMAGE_PREVIEW)
                        .fillMaxSize()
                        .clickable { selectedImage = null },
                    selectedImage = it
                )
            }
        }
    }
}