package com.example.imagepickerm3.ui.screens

import android.net.Uri
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color

@Composable
fun ImageSelected(
    modifier: Modifier = Modifier,
    selectedImage: Uri
) {
    Box(modifier = modifier.background(Color.Black)) {
        GalleryImage(
            modifier = Modifier.fillMaxWidth(),
            uri = selectedImage
        )
    }
}