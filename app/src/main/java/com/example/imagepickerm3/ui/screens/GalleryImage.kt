package com.example.imagepickerm3.ui.screens

import android.net.Uri
import androidx.compose.foundation.Image
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import coil.compose.rememberAsyncImagePainter
import coil.request.ImageRequest
import com.example.imagepickerm3.R

@Composable
fun GalleryImage(
    modifier: Modifier = Modifier,
    uri: Uri
) {
    /* if there is a custom ContentScale in the Image composable, i must set the same
     content scale inside rememberAsyncImagePainter() */
    val painter = rememberAsyncImagePainter(
        model = ImageRequest.Builder(LocalContext.current)
            .data(uri)
            .placeholder(R.drawable.ic_launcher_foreground)
            .crossfade(true)
            .error(R.drawable.ic_launcher_background)
            .build(),
        contentScale = ContentScale.Crop
    )

    Image(
        modifier = modifier,
        painter = painter,
        contentDescription = null,
        contentScale = ContentScale.Crop
    )
}