package com.example.imagepickerm3

import android.net.Uri
import androidx.compose.ui.test.assertIsDisplayed
import androidx.compose.ui.test.junit4.createComposeRule
import androidx.compose.ui.test.onNodeWithTag
import androidx.compose.ui.test.onNodeWithText
import androidx.compose.ui.test.performClick
import androidx.test.platform.app.InstrumentationRegistry
import com.example.imagepickerm3.data.Image
import com.example.imagepickerm3.ui.screens.GalleryContent
import com.example.imagepickerm3.utils.Tag.TAG_DENIED_PERMISSION
import com.example.imagepickerm3.utils.Tag.TAG_IMAGE_GALLERY
import com.example.imagepickerm3.utils.Tag.TAG_PROGRESS
import com.google.accompanist.permissions.ExperimentalPermissionsApi
import com.google.accompanist.permissions.PermissionState
import com.google.accompanist.permissions.PermissionStatus
import com.google.accompanist.permissions.isGranted
import com.google.accompanist.permissions.shouldShowRationale
import org.junit.Rule
import org.junit.Test
import org.mockito.kotlin.doReturn
import org.mockito.kotlin.mock
import org.mockito.kotlin.verify
import org.mockito.kotlin.whenever

@ExperimentalPermissionsApi class GalleryContentTest {
    @get:Rule
    val composeTestRule = createComposeRule()

    private val permissionState = object : PermissionState {
        override val permission: String
            get() = ""
        override val status: PermissionStatus
            /* change to true when needed PermissionStatus.Granted*/
            get() = PermissionStatus.Denied(true)

        override fun launchPermissionRequest() {}
    }

    private fun getTestString(id: Int) =
        InstrumentationRegistry.getInstrumentation().targetContext.getString(id)

    @Test
    fun assert_Image_Gallery_Displayed() {
        composeTestRule.setContent {
            GalleryContent(
                media = listOf(Image(id = 0, uri = Uri.EMPTY, name = "")),
                permissionState = permissionState,
                openSettings = {}
            )
        }

        composeTestRule.onNodeWithTag(TAG_IMAGE_GALLERY).assertIsDisplayed()
    }

    @Test
    fun assert_Progress_Displayed() {
        composeTestRule.setContent {
            GalleryContent(permissionState = permissionState, openSettings = {})
        }

        composeTestRule.onNodeWithTag(TAG_PROGRESS).assertIsDisplayed()
    }

    @Test
    fun assert_Denied_Permission_Displayed() {
        composeTestRule.setContent {
            GalleryContent(permissionState = permissionState, openSettings = {})
        }

        composeTestRule.onNodeWithTag(TAG_DENIED_PERMISSION).assertIsDisplayed()
    }

    @Test
    fun assert_Permission_Request_Triggered() {
        val state = mock<PermissionState>()
        whenever(state.status.isGranted)
            .doReturn(false)
        whenever(state.status.shouldShowRationale)
            .doReturn(true)

        composeTestRule.setContent {
            GalleryContent(
                media = null,
                permissionState = state,
                openSettings = {}
            )
        }

        val grantPermission = getTestString(R.string.permission_explainer_action)

        composeTestRule.onNodeWithText(grantPermission)
            .performClick()
        verify(state).launchPermissionRequest()
    }
}