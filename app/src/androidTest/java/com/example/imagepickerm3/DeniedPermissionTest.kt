package com.example.imagepickerm3

import androidx.compose.ui.test.assertIsDisplayed
import androidx.compose.ui.test.junit4.createComposeRule
import androidx.compose.ui.test.onNodeWithTag
import androidx.compose.ui.test.onNodeWithText
import androidx.compose.ui.test.performClick
import androidx.test.platform.app.InstrumentationRegistry
import com.example.imagepickerm3.ui.screens.DeniedPermission
import com.example.imagepickerm3.utils.Tag.TAG_PERMISSIONS_BUTTON
import org.junit.Rule
import org.junit.Test
import org.mockito.kotlin.mock
import org.mockito.kotlin.verify

class DeniedPermissionTest {
    @get:Rule
    val composeTestRule = createComposeRule()

    private fun getTestString(id: Int) =
        InstrumentationRegistry.getInstrumentation().targetContext.getString(id)

    @Test
    fun assert_Message_Displayed() {
        composeTestRule.setContent { DeniedPermission(handleLaunchSettings = {}) }

        composeTestRule.onNodeWithText(getTestString(R.string.permission_message))
            .assertIsDisplayed()
    }

    @Test
    fun assert_Action_Displayed() {
        composeTestRule.setContent { DeniedPermission(handleLaunchSettings = {}) }

        composeTestRule.onNodeWithTag(TAG_PERMISSIONS_BUTTON).assertIsDisplayed()
    }

    @Test
    fun assert_Action_Triggers_Callback() {
        val handleLaunchSettings: () -> Unit = mock()

        composeTestRule.setContent {
            DeniedPermission(handleLaunchSettings = handleLaunchSettings)
        }

        composeTestRule.onNodeWithTag(TAG_PERMISSIONS_BUTTON).performClick()

        verify(handleLaunchSettings).invoke()
    }
}