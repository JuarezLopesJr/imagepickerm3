package com.example.imagepickerm3

import android.net.Uri
import androidx.compose.ui.test.assert
import androidx.compose.ui.test.assertIsDisplayed
import androidx.compose.ui.test.hasTestTag
import androidx.compose.ui.test.junit4.createComposeRule
import androidx.compose.ui.test.onChildAt
import androidx.compose.ui.test.onNodeWithTag
import androidx.compose.ui.test.performClick
import com.example.imagepickerm3.data.Image
import com.example.imagepickerm3.ui.screens.ImageGallery
import com.example.imagepickerm3.utils.Tag.TAG_IMAGE
import com.example.imagepickerm3.utils.Tag.TAG_IMAGE_GRID
import com.example.imagepickerm3.utils.Tag.TAG_IMAGE_PREVIEW
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class ImageGalleryTest {
    @get:Rule
    val composeTestRule = createComposeRule()

    private val media = listOf(
        Image(0, Uri.EMPTY, ""),
        Image(1, Uri.EMPTY, "")
    )

    @Before
    fun setComposeTestRule() {
        composeTestRule.setContent {
            ImageGallery(images = media)
        }
    }

    @Test
    fun assert_Images_Displayed() {
        media.forEachIndexed { index, image ->
            composeTestRule.onNodeWithTag(TAG_IMAGE_GRID)
                .onChildAt(index)
                .assert(hasTestTag(TAG_IMAGE + image.id))
        }
    }

    @Test
    fun assert_Preview_Displayed() {
        composeTestRule.onNodeWithTag(TAG_IMAGE_GRID)
            .onChildAt(0)
            .performClick()

        composeTestRule.onNodeWithTag(TAG_IMAGE_PREVIEW)
            .assertIsDisplayed()
    }
}